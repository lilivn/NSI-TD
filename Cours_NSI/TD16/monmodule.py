import requests
"""
Ce module permet d'avoir quelques informations météorologiques grâce à une clé API.
"""
api_key = "c21a75b667d6f7abb81f118dcf8d4611"

def meteo(lieu):
    """
Renvoie toute les données météorologique d'un lieu, grâce à la clé API, sous forme de dictionnaire (Json).
param a: api_key -> la clé issue du compte api pour accéder aux données météorologique.
param b: lieu -> le lieu dont on veut savoir la météo.
return: fichier Json -> il contient toutes les données météo du lieu choisi

Doctests:
Impossible d'obtenir des doctests car les données sont aléatoires et varient tout le temps.

"""
    url = f"https://api.openweathermap.org/data/2.5/weather?q={lieu},fr&appid=c21a75b667d6f7abb81f118dcf8d4611&units=metric"
    r = requests.get(url)
    return r.json()

def vent(lieu):
    """
Renvoie la vitesse du vent d'un lieu.
param a: api_key -> la clé issue du compte api pour accéder aux données météorologique.
param b: lieu -> le lieu dont on veut savoir la vitesse du vent.
return: float -> la vitesse du vent en km/h.

Doctests:
Impossible d'obtenir des doctests car les données sont aléatoires et varient tout le temps.
"""
    print(meteo(lieu)['wind']['speed'])

def temperature(lieu):
    """
Renvoie la température d'un lieu.
param a: api_key -> la clé issue du compte api pour accéder aux données météorologique.
param b: lieu -> le lieu dont on veut savoir la température.
return: float -> la température qu'il fait dans le lieu.

Doctests:
Impossible d'obtenir des doctests car les données sont aléatoires et varient tout le temps.
"""
    print(meteo(lieu)['main']['temp'])
    
def ciel(lieu):
    """
Renvoie les conditions météorologique, si il pleut ou si il y a des nuages par exemple.
param a: api_key -> la clé issue du compte api pour accéder aux données météorologique.
param b: lieu -> le lieu dont on veut savoir la météo.
return: word -> la condition météorologique.

Doctests:
Impossible d'obtenir des doctests car les données sont aléatoires et varient tout le temps.
"""
    print(meteo(lieu)['weather'][0]['main'])
    
